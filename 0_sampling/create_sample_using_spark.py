import pandas
from pyspark.sql import SparkSession
import pyspark.sql.functions as sf

spark = SparkSession \
 .builder \
 .appName("Sampling") \
 .config("spark.dynamicAllocation.enabled", "true") \
 .getOrCreate()

biz = spark.read.json("../data/business.json")
usr = spark.read.json("../data/user.json") 
rev = spark.read.json("../data/review.json") 

biz = biz.withColumn('category', sf.concat_ws(',', 'categories'))

biz.registerTempTable('business')
usr.registerTempTable('user')
rev.registerTempTable('review')

sql = """ 
        SELECT *
        FROM review r
            LEFT JOIN user u 
                ON u.user_id=r.user_id
            LEFT JOIN (SELECT * 
                       FROM business
                       WHERE 
                       LOWER(category) like '%restaraunt%'
                        OR  LOWER(category) like '%breweries%'
                        OR  LOWER(category) like '%bars%'
                        OR  LOWER(category) like '%bagels%'
                        OR  LOWER(category) like '%donuts%'
                        OR  LOWER(category) like '%pizza%') b
                ON b.business_id=r.business_id
      """



pop = spark.sql(sql)
#DISK_ONLY = StorageLevel(True, False, False, False)
pop.registerTempTable('population')

sample = spark.sql("SELECT * FROM population WHERE state='PA'").sample(False, 0.25)

print(spark.sql("SELECT state, count(*) FROM population GROUP BY 1 ORDER BY 2 DESC").show(50, False))

sample.toPandas().to_csv('sample.csv',index=False) 

