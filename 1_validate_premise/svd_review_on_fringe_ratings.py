"""
Goal : To explore the basic structure of a simple model using the defaul star rating. 
Setup xargv fork of this file to test future models and pass them to a readable output.


---- Surprise Docs Worth Reading ----
http://surprise.readthedocs.io/en/stable/similarities.html#module-surprise.similarities
http://surprise.readthedocs.io/en/stable/accuracy.html
"""

from surprise import Dataset, SVD, Reader
from surprise.model_selection import cross_validate
from surprise.model_selection import train_test_split
from surprise import accuracy, similarities
import pandas as pd

df = pd.read_csv('../0_sampling/sample.csv')

normal = df[['user_id','business_id','stars']]

algo = SVD()

# Other Algorithm Features
# print('algo.pu - user factors')
# print(algo.pu)
# print('algo.qi - item factors')
# print(algo.qi)
# print('algo.bu - user biases')
# print(algo.bu)
# print('algo.bi - item biases')
# print(algo.bi)


reader = Reader(rating_scale=(1,5))
data = Dataset.load_from_df(normal, reader)
trainset, testset = train_test_split(data, test_size=.25)

algo.fit(trainset)
predictions = algo.test(testset)

print(pd.DataFrame(predictions))

## uid                     iid  r_ui       est  \
##  0      Z3208aKV0c-sgXZ4LNW42g  lpxj6LFir23Ds6swW8a6fg   5.0  3.181545
##  1      El74G4tjD8wjOtOSGcGd7A  mt9mrG8wALTzD3YYGim3mQ   2.0  3.302260
##  2      j6Kv_wLQzZMWobLIyijX3A  pklva9BUEvq3jtPL8Y8KDQ   4.0  4.422217
##  3      R2uJROFK5-sYXnPQeJdpzg  kfmvbmTIlkNpdWcHS5f1OA   1.0  3.582603
##  4      Wj5OhZQvVu4XTyhbJ22Lfg  9Lu2VvdAPswV-RRiQFfXXQ   5.0  2.673286
##  5      QwgXJvwctxwaDBIYoKteoQ  XMNdO9lFw3lSPO4gCH_qvQ   3.0  3.936039
##  6      dKKTpZ8TeX8qv4gT_otDHQ  w_UCGMgok7N9p0XdYBx1VQ   2.0  2.795150
##  7      C0MSJ65yKiJWdD1y7Io19Q  a5xUI7TvsNhM5gZr6KowDg   4.0  3.631418
##  8      hHFSpKcnDtJOs6M-JKFZ8Q  S9kueVeCoWHSO5TNjpHlcw   5.0  3.929780
##  9      BqUKr9JznUDJXw7gTy0dGA  Bqz24USKl4Rpb2nfGKo1XA   5.0  4.113870


