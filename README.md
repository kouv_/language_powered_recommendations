# Language Powered Reviews

## Arun Updated 6/20/18

### Overview

We should create the two metrics for validation.

### Sentiment Numeric Rating for Each comment and Average on User + Restaraunt 
  User average behavior, what the rating on our recommendation was
  
  A formula that uses the user's average rating, restaraunt average, and return a value that measures the lift of our recommendation.

# To Do
1. Create functions that take (test, predicted) and outputs accuracy metrics
  a. RMSE, MAE, Log Loss, Confusion, % Correct
2. Stratified sample based on total review count
3. NLP methods
## Additional Measurement
4.  Folks we would have recommend, # we would have secured using this recommendation.
  % of people we would have targeted, how many would have commented.
  weighted mean
  logloss
  star distribution
  user distribution compared to recommended distribution
  Average Rating per Restaraunt BEFORE - AFTER
  Sentiment of future rating after recommendation
  


### The Game Plane
![alt text](https://github.com/htpeter/language_powered_recommendations/blob/master/images/really_good_image.png)

### Important Notation for Recommender Models
![alt text](https://github.com/htpeter/language_powered_recommendations/blob/master/images/math_notation.png)
