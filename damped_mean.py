import pandas as pd

def damped_mean(input_df: pd.DataFrame, business: str='business', rating: str='rating', k:float=5) -> pd.DataFrame:
    """
    Implementation of Damped Mean Formula which is for implementaion of low confidence with few ratings. The mean rating
    is penalized for the less number of users providing the rating. 
        
    Arguments:
    input_df -- Dataframe containing the business, user and rating information
    business -- Column heading of the column in the input_df dataframe containing business ID. Default is 'business'
    rating -- Column heading of the column in the input_df dataframe containing rating given by the user to the business. Default is 'rating'
    k -- Strength of the evidence required to overcome the overall mean(Default equals 5)
       
    Returns:
    df_damped_mean -- Data Frame containing the business, Mean_rating, Damped_mean and count_rating
    """
    
    # reference - https://medium.com/@tomar.ankur287/non-personalised-recommender-system-in-python-42921cd6f971
    Ratings_mean=input_df.groupby([business])[[rating]].mean().rename(columns = {rating: 'Mean_rating'}).reset_index()

    Ratings_sum=input_df.groupby([business])[[rating]].sum().rename(columns = {rating: 'sum_rating'}).reset_index()

    Ratings_sum['sum_rating_factor']=Ratings_sum['sum_rating']+ k*(input_df[rating].mean())

    Ratings_count=input_df.groupby([business])[[rating]].count().rename(columns = {rating: 'count_rating'}).reset_index()

    Ratings_count['count_rating_factor']=Ratings_count['count_rating']+k

    Ratings_damped=pd.merge(Ratings_sum,Ratings_count,on=[business],how='left')

    Ratings_damped['Damped_mean']=Ratings_damped['sum_rating_factor']/Ratings_damped['count_rating_factor']

    Ratings_mean_dampmean=pd.merge(Ratings_mean[[business,'Mean_rating']],Ratings_damped[[business,'Damped_mean', 'count_rating']],on=[business],how='left')

    return Ratings_mean_dampmean
