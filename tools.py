"""
Code that supports rapid research of NLP
methods applied to reccomendation methods.
"""
from typing import Dict
import pandas as pd
from sklearn.metrics import accuracy_score, log_loss

def class_measure(real: pd.DataFrame, pred: pd.DataFrame) -> pd.DataFrame:
    """
    Base measurement class that calculates and returns
    multiple metrics useful in determining model success. 
    """
    assert isinstance(real, pd.DataFrame)
    assert isinstance(pred, pd.DataFrame)
    assert set(real.columns) == set(['stars','user_id','business_id'])
    assert set(pred.columns) == set(['stars','user_id','business_id'])
    
    df = pd.merge(real, pred, how='left', left_on=['user_id','business_id'],
                    right_on=['user_id','business_id'])
    
    # 'stars_x' is real, 'stars_y' is pred

    storage = pd.DataFrame({'measure_name' : [], 'metric_value' : []})
    
    ### overall accuracy ###

    storage = storage.append(pd.DataFrame({'measure_name' : ['accuracy score'],
                                 'metric_value' : [accuracy_score(df.stars_x,df.stars_y)]}),
                                 ignore_index=True)

    ### star level accuracy ###

    for i in df.stars_x.unique():
        tmp = df[df.stars_x == i]
        score = pd.DataFrame({'measure_name' : [str(i)+'star_score'],
                              'metric_value' : [accuracy_score(tmp.stars_x,tmp.stars_y)]})
        storage = storage.append(score, ignore_index=True)
    
    ### return ###

    return storage

def continuous_measure(real: pd.DataFrame, pred: pd.DataFrame) -> pd.DataFrame:
    """
    Base measurement class that calculates and returns
    multiple metrics useful in determining model success. 
    """
    assert isinstance(real, pd.DataFrame)
    assert isinstance(pred, pd.DataFrame)
    assert set(real.columns) == set(['stars','user_id','business_id'])
    assert set(pred.columns) == set(['stars','user_id','business_id'])
    
    df = pd.merge(real, pred, how='left', left_on=['user_id','business_id'],
                    right_on=['user_id','business_id'])
    
    # 'stars_x' is real, 'stars_y' is pred

    df.stars_x = df.stars_x.map(lambda x: round(x,0))
    df.stars_y = df.stars_y.map(lambda x: round(x,0))
    
    storage = pd.DataFrame({'measure_name' : [], 'metric_value' : []})
    
    ### overall accuracy ###

    storage = storage.append(pd.DataFrame({'measure_name' : ['accuracy score'],
                                 'metric_value' : [accuracy_score(df.stars_x,df.stars_y)]}),
                                 ignore_index=True)

    ### star level accuracy ###

    for i in df.stars_x.unique():
        tmp = df[df.stars_x == i]
        score = pd.DataFrame({'measure_name' : [str(i)+'star_score'],
                              'metric_value' : [accuracy_score(tmp.stars_x,tmp.stars_y)]})
        storage = storage.append(score, ignore_index=True)
    
    ### return ###

    return storage



if __name__=='__main__':

    t1 = pd.DataFrame({'stars' : [5.1, 3.2, 4.6],
                       'user_id' : ['u1', 'u2', 'u2'],
                       'business_id' : ['b1', 'b1', 'b2']})
    t2 = pd.DataFrame({'stars' : [5, 5, 4],
                       'user_id' : ['u1', 'u2', 'u2'],
                       'business_id' : ['b1', 'b1', 'b2']})
    
    z = continuous_measure(t1, t2)
    print(z)
